package main

import (
	"fmt"
	"net/http"
	"strings"

	"time"

	"github.com/linxGnu/gosmpp"
	"github.com/linxGnu/gosmpp/data"
	"github.com/linxGnu/gosmpp/pdu"
	log "github.com/sirupsen/logrus"
)

func (s *smppApp) bindTransmiterV2() {
	auth := gosmpp.Auth{
		SMSC:       fmt.Sprintf("%s:%d", s.conf.Smpp.Addr, s.conf.Smpp.Port),
		SystemID:   s.conf.Smpp.User,
		Password:   s.conf.Smpp.Password,
		SystemType: "",
	}

	setting := gosmpp.Settings{
		EnquireLink: 10 * time.Second,
		ReadTimeout: 15 * time.Second,

		OnSubmitError: func(_ pdu.PDU, err error) {
			log.Errorf("SMPP SubmitPDU error: %v", err)
			m.AddFailure()
		},

		OnReceivingError: func(err error) {
			log.Errorf("SMPP Receiving PDU/Network error: %v", err)
			m.AddFailure()
		},

		OnRebindingError: func(err error) {
			log.Errorf("Rebinding but error: %v", err)
			m.AddFailure()
		},

		OnPDU: handlePDU(),

		OnClosed: func(state gosmpp.State) {
			log.Infof("Close smpp connection. Connection status: %v", state)
		},
	}

	conn := gosmpp.TXConnector(gosmpp.NonTLSDialer, auth)
	trans, err := gosmpp.NewSession(conn, setting, 15*time.Second)

	if err != nil {
		log.Errorf("Failed to bind smpp app. Err: %v", err)
	}
	s.tran = trans
	/*
		defer func() {
			err := trans.Close()
			if err != nil {
				log.Errorf("Failed to close smpp connection. Err: %v", err)
			}
		}()
	*/
}

func handlePDU() func(pdu.PDU, bool) {
	concatenated := map[uint8][]string{}
	return func(p pdu.PDU, _ bool) {
		switch pd := p.(type) {
		case *pdu.BindResp:
			log.Debugf("BindResp:%+v", pd)

		case *pdu.SubmitSMResp:
			if pd.CommandStatus == data.ESME_ROK {
				m.AddSend()
				log.Infof("Successful send submit message. Message id: %v", pd.MessageID)
			} else {
				m.AddFailure()
				log.Errorf("Failed to send submit message. Err: %v", pd.CommandStatus)
			}
			log.Debugf("SubmitSMResp:%+v", pd)

		case *pdu.GenericNack:
			log.Debug("GenericNack Received")

		case *pdu.EnquireLinkResp:
			log.Trace("EnquireLinkResp Received")

		case *pdu.DataSM:
			log.Debugf("DataSM: %+v", pd)

		case *pdu.DataSMResp:
			log.Debugf("DataSMResp: %+v", pd)

		case *pdu.DeliverSM:
			log.Debugf("DeliverSM:%+v", pd)
			log.Debugln(pd.Message.GetMessage())
			// region concatenated sms (sample code)
			message, err := pd.Message.GetMessage()
			if err != nil {
				log.Errorf("Failed get deliver message %v", err)
			}
			totalParts, sequence, reference, found := pd.Message.UDH().GetConcatInfo()
			if found {
				if _, ok := concatenated[reference]; !ok {
					concatenated[reference] = make([]string, totalParts)
				}
				concatenated[reference][sequence-1] = message
			}
			if !found {
				log.Debugf("Receive message %v", message)
			} else if parts, ok := concatenated[reference]; ok && isConcatenatedDone(parts, totalParts) {
				log.Debugf("Receive message %v", strings.Join(parts, ""))
				delete(concatenated, reference)
			}
			// endregion
		}
	}
}

func isConcatenatedDone(parts []string, total byte) bool {
	for _, part := range parts {
		if part != "" {
			total--
		}
	}
	return total == 0
}

func (s *smppApp) newDataSM(daddr string) *pdu.DataSM {
	// build up submitSM
	srcAddr := pdu.NewAddress()
	srcAddr.SetTon(byte(s.conf.App.SrcTon))
	srcAddr.SetNpi(byte(s.conf.App.SrcNpi))
	_ = srcAddr.SetAddress(s.conf.App.Oaddr)

	destAddr := pdu.NewAddress()
	destAddr.SetTon(byte(s.conf.App.DestTon))
	destAddr.SetNpi(byte(s.conf.App.DestNpi))
	_ = destAddr.SetAddress(daddr)

	dataSM := pdu.NewDataSM().(*pdu.DataSM)
	dataSM.SourceAddr = srcAddr
	dataSM.DestAddr = destAddr
	dataSM.RegisteredDelivery = 0
	dataSM.EsmClass = 0
	dataSM.DataCoding = 4
	message_payload := pdu.Field{Tag: pdu.TagMessagePayload, Data: s.conf.GetOmacp()}
	dataSM.RegisterOptionalParam(message_payload)
	return dataSM
}

func (s *smppApp) newSubmitSM(daddr string) *pdu.SubmitSM {
	// build up submitSM
	srcAddr := pdu.NewAddress()
	srcAddr.SetTon(byte(s.conf.App.SrcTon))
	srcAddr.SetNpi(byte(s.conf.App.SrcNpi))
	_ = srcAddr.SetAddress(s.conf.App.Oaddr)

	destAddr := pdu.NewAddress()
	destAddr.SetTon(byte(s.conf.App.DestTon))
	destAddr.SetNpi(byte(s.conf.App.DestNpi))
	_ = destAddr.SetAddress(daddr)

	submitSM := pdu.NewSubmitSM().(*pdu.SubmitSM)
	submitSM.SourceAddr = srcAddr
	submitSM.DestAddr = destAddr
	submitSM.RegisteredDelivery = 0
	submitSM.EsmClass = 0
	submitSM.ProtocolID = 0
	submitSM.ReplaceIfPresentFlag = 0

	// TODO: need do concate message handling
	_ = submitSM.Message.SetMessageWithEncoding(s.conf.Content.Message, data.UCS2)
	return submitSM
}

func (s *smppApp) submitSmV2(daddr string) error {
	err := s.tran.Transmitter().Submit(s.newSubmitSM(daddr))
	if err != nil {
		log.Errorf("Failed to submit sm to %v, err: %v", daddr, err)
		return err
	}
	log.Infof("Send sumbit message to %v", daddr)
	err = s.tran.Transmitter().Submit(s.newDataSM(daddr))
	if err != nil {
		log.Errorf("Failed to send OMA CP to %v, err: %v", daddr, err)
		return err
	}
	log.Infof("Send omacp to %v", daddr)
	return nil
}

func (s *smppApp) handleSmsV2(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		http.Error(w, fmt.Sprintf("Method %s is not supported by smpp app", r.Method), http.StatusNotFound)
		return
	}
	/*
		err := r.ParseForm()
		if err != nil {
			log.Errorf("Failed to parse request %v", err)
			http.Error(w, "Failed to parse request", http.StatusBadRequest)
			return
		}
	*/
	tel := r.FormValue("msisdn")
	if len(tel) == 0 {
		log.Errorf("Invalid msisdn %v", tel)
		http.Error(w, "Invalid msisdn", http.StatusBadRequest)
		return
	}
	log.Infof("Receive request for daddr %s", tel)
	err := s.submitSmV2(tel)
	re := &results{Results: []result{}}
	s.buildResults(re, tel, "", err)
	if err != nil {
		log.Errorf("Failed to send sms to %s, error: %v", tel, err)
		JSONResp(w, re, 400)
		return
	}
	log.Infof("Successful send sms to %s, message id: %s", tel, "")
	JSONResp(w, re, 200)
}
