package main

import (
	"time"

	"github.com/linxGnu/gosmpp"
	"github.com/rcrowley/go-metrics"
	"github.com/skill215/go-smpp/smpp"
)

type telList struct {
	User     string       `json:"user"`
	Passowrd string       `json:"password"`
	Inputs   []MsisdnImsi `json:"inputs"`
}

type MsisdnImsi struct {
	Msisdn string `json:"msisdn"`
	Imsi   string `json:"imsi"`
}

type results struct {
	Results []result `json:"results"`
}

type result struct {
	Msisdn  string `json:"msisdn"`
	Imsi    string `json:"imsi"`
	Status  string `json:"status"`
	Message string `json:"message"`
}

type statResult struct {
	Timestamp time.Time    `json:"timestamp"`
	StartTime time.Time    `json:"start"`
	Metrics   MetricResult `json:"metrics"`
}

type MetricResult struct {
	Count []CounterAndMeter `json:"counters"`
}

type CounterAndMeter struct {
	Name     string `json:"name"`
	Count    uint64 `json:"count"`
	Avg      uint64 `json:"avg"`
	Avg1min  uint64 `json:"avg1min"`
	Avg5min  uint64 `json:"avg5min"`
	Avg15min uint64 `json:"avg15min"`
}

type stat struct {
	Start  time.Time
	meters map[string]metrics.Meter
}

type smppApp struct {
	conf  *config
	tx    *smpp.Transmitter
	rc    *smpp.Receiver
	tr    *smpp.Transceiver
	tran  *gosmpp.Session
	conn  <-chan smpp.ConnStatus
	msgch chan interface{}
}

type loopConf struct {
	Tps int  `json:"tps"`
	Srr bool `json:"srrequest"`
}

type config struct {
	Smpp struct {
		Addr      string `yaml:"addr"`
		Port      int    `yaml:"port"`
		User      string `yaml:"user"`
		Password  string `yaml:"password"`
		Type      string `yaml:"type"`
		ConnNum   int    `yaml:"connections"`
		RequireSR bool   `yaml:"requireStatusReport"`
		Tls       struct {
			Enable       bool   `yaml:"enable"`
			Key          string `yaml:"key"`
			Cert         string `yaml:"cert"`
			Ciphers      string `yaml:"ciphers"`
			MinVersion   string `yaml:"minVersion"`
			MaxVersion   string `yaml:"maxVersion"`
			VerifyServer bool   `yaml:"verifyServerCert"`
			CaCert       string `yaml:"ca"`
			ServerName   string `yaml:"serverName"`
		} `yaml:"tls"`
	} `yaml:"smpp"`
	App struct {
		LogLevel     string `yaml:"logLevel"`
		SrcTon       int    `yaml:"src_ton"`
		SrcNpi       int    `yaml:"src_npi"`
		DestTon      int    `yaml:"dst_ton"`
		DestNpi      int    `yaml:"dst_npi"`
		Oaddr        string `yaml:"oaddr"`
		DaddrPrefix  string `yaml:"daddrPrefix"`
		Version      int    `yaml:"version"`
		LatencyPrint int64  `yaml:"latency-print"`
	} `yaml:"app"`
	Rest struct {
		Port     int    `yaml:"port"`
		User     string `yaml:"user"`
		Password string `yaml:"password"`
	} `yaml:"rest"`
	Metrics struct {
		Interval int    `yaml:"interval"`
		Output   string `yaml:"output"`
		User     string `yaml:"user"`
	} `yaml:"metrics"`
	Content struct {
		Message   string `yaml:"message"`
		OmacpPath string `yaml:"omacp"`
		WapPush   struct {
			SourcePort uint16 `yaml:"source_port"`
			DestPort   uint16 `yaml:"destination_port"`
		} `yaml:"wap_push"`
	} `yaml:"contents"`
}
