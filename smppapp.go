package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
	"time"
	"unicode"

	log "github.com/sirupsen/logrus"
	"github.com/skill215/go-smpp/smpp"
	"github.com/skill215/go-smpp/smpp/pdu/pdufield"
	"github.com/skill215/go-smpp/smpp/pdu/pdutext"
	"github.com/skill215/go-smpp/smpp/pdu/pdutlv"
)

// build the json struct for response
func (s *smppApp) buildResults(r *results, msisdn string, id string, err error) {
	re := result{Msisdn: msisdn}
	if err != nil {
		re.Message = err.Error()
		re.Status = "failed"
	} else {
		re.Status = "success"
		re.Message = id
	}
	r.Results = append(r.Results, re)
}

// init smpp transmiter with smpp server addr and user/password
func (s *smppApp) initSmppTransmiter() {
	s.tx = &smpp.Transmitter{
		Addr:   fmt.Sprintf("%s:%d", s.conf.Smpp.Addr, s.conf.Smpp.Port),
		User:   s.conf.Smpp.User,
		Passwd: s.conf.Smpp.Password,
	}
	if s.conf.isTlsEnabled() {
		s.setTLS()
	}
}

// bind transmiter, enquirelink default set to 10 seconds
func (s *smppApp) bindSmpp() error {
	switch s.conf.SmppType() {
	case "receiver":
		return s.bindReceiver()
	case "transmiter":
		return s.bindTransmiter()
	case "transceiver":
		return s.bindTransceiver()
	default:
		return fmt.Errorf("smpp bind type error")
	}
}

func (s *smppApp) bindTransceiver() error {
	s.tr = &smpp.Transceiver{
		Addr:   fmt.Sprintf("%s:%d", s.conf.Smpp.Addr, s.conf.Smpp.Port),
		User:   s.conf.Smpp.User,
		Passwd: s.conf.Smpp.Password,
	}
	if s.conf.isTlsEnabled() {
		s.setTLS()
	}
	s.conn = s.tr.Bind()
	s.tr.Handler = handlerAT
	var status smpp.ConnStatus
	if status = <-s.conn; status.Error() != nil {
		log.Fatalf("Unable to connect to SMPP server, aborting: %v", status.Error())
		return fmt.Errorf("Unable to connect to SMPP server, aborting: %v", status.Error())
	}
	log.Infof("Bind transceiver connection completed, status: %v", status.Status())
	m.Start = time.Now()
	return nil
}

func (s *smppApp) bindTransmiter() error {
	s.initSmppTransmiter()
	conn := s.tx.Bind()
	var status smpp.ConnStatus
	if status = <-conn; status.Error() != nil {
		log.Fatalf("Unable to connect to SMPP server, aborting: %v", status.Error())
		return fmt.Errorf("Unable to connect to SMPP server, aborting: %v", status.Error())
	}
	log.Infof("Bind transmiter connection completed, status: %v", status.Status())
	return nil
}

func (s *smppApp) bindReceiver() error {
	s.rc = &smpp.Receiver{
		Addr:   fmt.Sprintf("%s:%d", s.conf.Smpp.Addr, s.conf.Smpp.Port),
		User:   s.conf.Smpp.User,
		Passwd: s.conf.Smpp.Password,
	}
	if s.conf.isTlsEnabled() {
		s.setTLS()
	}
	s.conn = s.rc.Bind()
	s.rc.Handler = handlerAT
	var status smpp.ConnStatus
	if status = <-s.conn; status.Error() != nil {
		log.Fatalf("Unable to connect to SMPP server, aborting: %v", status.Error())
		return fmt.Errorf("Unable to connect to SMPP server, aborting: %v", status.Error())
	}
	log.Infof("Bind receiver connection completed, status: %v", status.Status())
	m.Start = time.Now()
	return nil
}

// example of connection checker goroutine
func (s *smppApp) checkStatus() {
	for c := range s.conn {
		log.Infof("SMPP connection status: %v", c.Status())
		if c.Status() == smpp.Disconnected|smpp.ConnectionFailed {
			log.Error("Connection to smpp server is broken")
		}
	}
}

// submit sm
func (s *smppApp) submitSm(oaddr string, daddr string, r *results) error {
	sms := smpp.ShortMessage{
		Dst:           daddr,
		SourceAddrTON: uint8(s.conf.App.SrcTon),
		SourceAddrNPI: uint8(s.conf.App.SrcNpi),
		DestAddrTON:   uint8(s.conf.App.DestNpi),
		DestAddrNPI:   uint8(s.conf.App.DestTon),
		Text:          pdutext.UCS2(s.conf.Content.Message),
	}
	if len(oaddr) > 0 {
		sms.Src = oaddr
	}
	if s.conf.Smpp.RequireSR {
		sms.Register = pdufield.FinalDeliveryReceipt
	} else {
		sms.Register = pdufield.NoDeliveryReceipt
	}
	var smlist []smpp.ShortMessage
	var err error
	startTime := time.Now().Local()
	if s.tx != nil {
		if len(s.conf.Content.Message) < 140 {
			sm, err1 := s.tx.Submit(&sms)
			err = err1
			smlist = []smpp.ShortMessage{*sm}
		} else {
			smlist, err = s.tx.SubmitLongMsg(&sms)
		}
	} else if s.tr != nil {
		if len(s.conf.Content.Message) < 140 {
			sm, err1 := s.tr.Submit(&sms)
			err = err1
			smlist = []smpp.ShortMessage{*sm}
		} else {
			smlist, err = s.tr.SubmitLongMsg(&sms)
		}
	}
	m.AddSend()
	if err != nil {
		m.AddFailure()
		return fmt.Errorf("SMPP connection error: %w", err)
	}

	for _, sm := range smlist {
		res := result{Msisdn: daddr}
		if sm.Resp().Header().Status != 0x00000000 {
			m.AddFailure()
			err = fmt.Errorf("submit SM failed, SMPP error: %s", sm.Resp().Header().Status.Error())
			res.Status = "Failed"
			res.Message = sm.Resp().Header().Status.Error()
		}
		latency := time.Since(startTime).Milliseconds()
		if latency >= conf.App.LatencyPrint {
			log.Infof("submit msg latency is %d", latency)
		}
		res.Status = "Success"
		res.Message = fmt.Sprintf("MessageID:%v", sm.RespID())
		r.Results = append(r.Results, res)
		log.Debugf("Response Message ID: %s, DADDR: %s, Status: %s", sm.RespID(), daddr, sm.Resp().Header().Status.Error())
	}

	return err
}

// 2 solutions for data_sm
// 1. use sar and src_port/dest_port in TLV to indicate the UDH. SMSC will automatically generate the UDH
// 2. generate UDH ourselves. That's a more common way.
func (s *smppApp) dataSm(oaddr string, daddr string, imsi string, r *results) error {
	maxLen := 128 // 140 bytes include UDH for concat message, 133 for single message
	content := s.conf.GetOmacp()
	// generate wireless session protocol header for omacp operation
	mspheader := GenerateWSPHeader(GenImsiBcd(imsi), s.conf.GetOmacp())
	// append wsp header to wbxml data
	payload := append(mspheader, content...)
	// calculate the concat message num
	var countParts int
	if len(payload) <= 133 { // only contains WAP PUSH UDH with length 7
		countParts = 1
	} else {
		countParts = (len(payload) / maxLen) + 1
	}
	for i := 0; i < countParts; i++ {
		log.Debugf("Building data_sm payload, total %d, seq %d", countParts, i)
		// generate UDH contains of concat info and src/dst port for wap push
		udh := generateUDH(uint8(countParts), uint8(i+1), s.conf.getSrcPort(), s.conf.getDestPort())
		var ud []byte
		if i != countParts-1 {
			ud = payload[i*maxLen : (i+1)*maxLen]
		} else {
			ud = payload[i*maxLen:]
		}
		ud = append(udh, ud...)
		dm := smpp.DataMessage{
			Src:           oaddr,
			Dst:           daddr,
			SourceAddrTON: uint8(s.conf.App.SrcTon),
			SourceAddrNPI: uint8(s.conf.App.SrcNpi),
			DestAddrTON:   uint8(s.conf.App.DestTon),
			DestAddrNPI:   uint8(s.conf.App.DestNpi),
			DataCoding:    pdutext.Binary2Type,
			Register:      pdufield.NoDeliveryReceipt,
			ESMClass:      0x40, // ESM class 0x40 means UDHI 0x01
			TLVFields: pdutlv.Fields{
				pdutlv.TagDestNetworkType: uint8(0x01), // GSM
				pdutlv.TagMessagePayload:  ud,
			},
		}
		// add more message to send to indicate concat messages
		if countParts > 1 && i != countParts-1 {
			dm.TLVFields[pdutlv.TagMoreMessagesToSend] = uint8(0x01)
		} else if i == countParts-1 {
			dm.TLVFields[pdutlv.TagMoreMessagesToSend] = uint8(0x00)
		}

		log.Debugf("data message is %v", dm)
		log.Tracef("message_payload: %+v", dm.TLVFields[pdutlv.TagMessagePayload])
		res := result{Msisdn: daddr, Imsi: imsi}
		dmr, err := s.tx.DataMsg(&dm)
		m.AddSend()
		if err != nil {
			m.AddFailure()
			return fmt.Errorf("SMPP connection error: %w", err)
		}

		if dmr.Resp().Header().Status != 0x00000000 {
			m.AddFailure()
			log.Errorf("data SM failed, SMPP error: %s", dmr.Resp().Header().Status.Error())
			res.Status = "Failed"
			res.Message = dmr.Resp().Header().Status.Error()
		} else {
			res.Status = "Success"
			res.Message = dmr.RespID()
		}
		r.Results = append(r.Results, res)
		log.Debugf("Data_sm Response Message ID: %s, Status: %s", dmr.RespID(), dmr.Resp().Header().Status.Error())
	}

	return nil
}

func (s *smppApp) setTLS() {
	log.Info("Configure TLS for smpp app")

	// set min version and max version
	c := s.conf.generateTlsConf()

	if s.conf.isReceiver() {
		s.rc.TLS = c
	} else {
		s.tx.TLS = c
	}
}

// handle single sm submit
func (s *smppApp) handleSms(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		http.Error(w, fmt.Sprintf("Method %s is not supported by smppapp", r.Method), http.StatusNotFound)
		return
	}

	err := r.ParseForm()
	if err != nil {
		log.Errorf("Failed to parse request %v", err)
		http.Error(w, "Failed to parse request", http.StatusBadRequest)
		return
	}
	user := r.FormValue("user")
	passwd := r.FormValue("password")

	if !s.conf.authenticate(user, passwd) {
		log.Errorf("failed to authenticate request %v", err)
		http.Error(w, "wrong username or invalid password", http.StatusUnauthorized)
		return
	}

	tel := r.FormValue("msisdn")
	err = checkMsisdn(tel)
	if err != nil {
		log.Error(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	imsi := r.FormValue("imsi")
	err = checkImsi(imsi)
	if err != nil {
		log.Error(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	log.Infof("Receive request for daddr %s, imsi %s", tel, imsi)
	re := &results{Results: []result{}}
	err = s.submitSm(s.conf.App.Oaddr, tel, re)

	if err != nil {
		log.Errorf("Failed to send sms to %s, error: %v", tel, err)
		JSONResp(w, re, 400)
		return
	}
	if s.conf.hasDataSm() {
		err = s.dataSm(s.conf.App.Oaddr, tel, imsi, re)
		if err != nil {
			log.Errorf("Failed to send oma cp to %s, error: %v", tel, err)
			JSONResp(w, re, 400)
			return
		}
	}
	JSONResp(w, re, 200)
}

func (s *smppApp) handleList(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		http.Error(w, fmt.Sprintf("Method %s is not supported by smppapp", r.Method), http.StatusNotFound)
		return
	}
	decoder := json.NewDecoder(r.Body)
	defer r.Body.Close()
	l := telList{}
	err := decoder.Decode(&l)
	if err != nil {
		log.Errorf("failed to decode incoming request %v", err)
		http.Error(w, "Request format error", http.StatusBadRequest)
		return
	}
	if !s.conf.authenticate(l.User, l.Passowrd) {
		log.Errorf("failed to authenticate request %v", err)
		http.Error(w, "wrong username or invalid password", http.StatusUnauthorized)
		return
	}
	timeBefore := time.Now()
	log.Infof("Receive requests for list %v", l.Inputs)
	re := &results{Results: []result{}}
	for _, input := range l.Inputs {
		if s.conf.hasMessage() {
			err := s.submitSm(s.conf.App.Oaddr, input.Msisdn, re)
			if err != nil {
				log.Errorf("Failed to send sms to %s, error: %v", input.Msisdn, err)
			} else {
				log.Infof("Successful send sms to %s", input.Msisdn)
			}
		}
		// send the data_sm
		if s.conf.hasDataSm() {
			err = s.dataSm(s.conf.App.Oaddr, input.Imsi, input.Imsi, re)
			if err != nil {
				log.Errorf("Failed to send data_sm to tel: %s imsi: %s, error: %v", input.Msisdn, input.Imsi, err)
			} else {
				log.Infof("Successful send data_sm to tel: %s imsi: %s", input.Msisdn, input.Imsi)
			}
		}
	}
	timeAfter := time.Now()
	log.Infof("Complete send list request. Time duration: %s", timeAfter.Sub(timeBefore).String())
	status := http.StatusOK
	if err != nil {
		status = http.StatusInternalServerError
	}
	JSONResp(w, re, status)
}

func isInt(s string) bool {
	for _, c := range s {
		if !unicode.IsDigit(c) {
			return false
		}
	}
	return true
}

func checkMsisdn(s string) error {
	s = strings.TrimSpace(s)
	if len(s) == 0 {
		return fmt.Errorf("Empty msisdn")
	}
	if len(s) > 15 {
		return fmt.Errorf("msisdn too long %s", s)
	}
	if s[0] == '+' {
		if isInt(s[1:]) {
			return nil
		}
		return fmt.Errorf("Invalid msisdn %s. Should be digits or +digits", s)
	} else if isInt(s) {
		return nil
	} else {
		return fmt.Errorf("Invalid msisdn %s. Should be digits or +digits", s)
	}
}

func checkImsi(s string) error {
	s = strings.TrimSpace(s)
	if len(s) == 0 {
		return fmt.Errorf("Empty imsi")
	} else {
		if isInt(s) {
			if len(s) > 15 {
				return fmt.Errorf("imsi too long %s", s)
			}
			return nil
		} else {
			return fmt.Errorf("Invalid imsi %s, should be all digits", s)
		}
	}
}

func generateUDH(sarTotal, sarSeq uint8, srcPort, destPort uint16) []byte {
	srcHighByte, srcLowByte := int16Tobytes(int16(srcPort))
	destHighByte, destLowByte := int16Tobytes(int16(destPort))
	// for concat message
	if sarTotal > 1 {
		sarRef := GetSeq()
		UDHHeader := make([]byte, 12)
		UDHHeader[0] = 0x0b         // length of user data header
		UDHHeader[1] = 0x05         // source port and destination port for wap push
		UDHHeader[2] = 0x04         // length of remaining header
		UDHHeader[3] = srcHighByte  // minor part of source port
		UDHHeader[4] = srcLowByte   // major part of source port
		UDHHeader[5] = destHighByte // minor part of dest port
		UDHHeader[6] = destLowByte  // major part of dest port
		UDHHeader[7] = 0x00         // information element identifier, CSMS 8 bit reference number
		UDHHeader[8] = 0x03         // length of remaining header
		UDHHeader[9] = sarRef       // the reference number
		UDHHeader[10] = sarTotal    // total number of message parts
		UDHHeader[11] = sarSeq      // sequence number of message part

		return UDHHeader
	} else {
		// for single message
		UDHHeader := make([]byte, 7)
		UDHHeader[0] = 0x06         // length of user data header
		UDHHeader[1] = 0x05         // 6source port and destination port for wap push
		UDHHeader[2] = 0x04         // length of remaining header
		UDHHeader[3] = srcHighByte  // minor part of source port hard code to 2948
		UDHHeader[4] = srcLowByte   // major part of source port
		UDHHeader[5] = destHighByte // minor part of dest port  hard code to 2948
		UDHHeader[6] = destLowByte  // major part of dest port
		return UDHHeader
	}
}

// input int16 ,output high byte and low byte
func int16Tobytes(i int16) (uint8, uint8) {
	return uint8(i >> 8), uint8(i & 0xff)
}
