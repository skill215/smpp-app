package main

import (
	"io"
	"time"

	log "github.com/sirupsen/logrus"
	lumberjack "gopkg.in/natefinch/lumberjack.v2"
)

// setup log file for rotate and max size
func SetupLogger() {
	logger := &lumberjack.Logger{
		// Log file abbsolute path, os agnostic
		Filename:   "rest4smpp.log",
		MaxSize:    5, // MB
		MaxBackups: 10,
		Compress:   true, // disabled by default
	}

	logFormatter := &log.TextFormatter{
		TimestampFormat:           time.RFC3339,
		FullTimestamp:             true,
		EnvironmentOverrideColors: true,
		PadLevelText:              true,
	}

	log.SetFormatter(logFormatter)
	log.SetOutput(io.Writer(logger))
}
