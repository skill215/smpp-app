package main

import (
	"time"

	"github.com/rcrowley/go-metrics"
)

// counter type
var (
	SEND_SM    = "send_sm"
	RECEIVE_SM = "receive_sm"
	FAILED_SM  = "failed_sm"
	counters   []string
)

func (s *stat) startSenderMetrics() {
	counters = append(counters, SEND_SM)
	counters = append(counters, FAILED_SM)
	counters = append(counters, RECEIVE_SM)
	s.start()
}

func (s *stat) startReceiverMetrics() {
	counters = append(counters, SEND_SM)
	counters = append(counters, FAILED_SM)
	counters = append(counters, RECEIVE_SM)
	s.start()
}

// start meter for 3 counters and register to metrics
func (s *stat) start() {
	s.Start = time.Now()
	s.meters = map[string]metrics.Meter{}

	for _, c := range counters {
		s.meters[c] = metrics.NewMeter()
		_ = metrics.Register(c, s.meters[c])
	}
}

// stop meters and unregister from metrics
func (s *stat) stop() {
	for _, c := range counters {
		s.meters[c].Stop()
		metrics.Unregister(c)
	}
}

func (s *stat) AddSend() {
	s.meters[SEND_SM].Mark(1)
}

func (s *stat) AddReceive() {
	s.meters[RECEIVE_SM].Mark(1)
}

func (s *stat) AddFailure() {
	s.meters[FAILED_SM].Mark(1)
}

func (s *stat) Reset() {
	s.stop()
	s.start()
}

func (s *stat) Result() *statResult {
	r := &statResult{
		Timestamp: time.Now(),
		StartTime: s.Start,
		Metrics:   *s.generateMetric(),
	}
	return r
}

func (s *stat) generateMetric() *MetricResult {
	m := &MetricResult{Count: []CounterAndMeter{}}
	for _, c := range counters {
		m.addResult(c, s)
	}
	return m
}

func (m *MetricResult) addResult(name string, s *stat) *MetricResult {
	meter := s.meters[name].Snapshot()
	c := CounterAndMeter{
		Name:     name,
		Count:    uint64(meter.Count()),
		Avg:      uint64(meter.RateMean()),
		Avg1min:  uint64(meter.Rate1()),
		Avg5min:  uint64(meter.Rate5()),
		Avg15min: uint64(meter.Rate15()),
	}
	m.Count = append(m.Count, c)
	return m
}
