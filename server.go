package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"math/rand"
	"net/http"
	"strconv"
	"time"

	log "github.com/sirupsen/logrus"
	"github.com/skill215/go-smpp/smpp/pdu"
)

var (
	h       bool
	c       string
	b       *Broker
	m       *stat
	smsChan chan string
	conf    *config
)

// Send Json in http response
func JSONResp(w http.ResponseWriter, resp interface{}, code int) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.Header().Set("X-Content-Type-Options", "nosniff")
	w.WriteHeader(code)
	json.NewEncoder(w).Encode(resp) //nolint:errcheck
}

func startLoop(w http.ResponseWriter, r *http.Request) {
	log.Info("Start performance testing.")
	tps, _ := strconv.Atoi(r.FormValue("tps"))
	b.Publish("stop")
	time.Sleep(50 * time.Millisecond)
	m.Reset()
	for i := 1; i <= conf.getConnNum(); i++ {
		go func(i int) {
			app := &smppApp{conf: conf, msgch: b.Subscribe()}
			err := app.bindSmpp()
			if err != nil {
				log.Fatalf("Failed to bind smpp app %v", err)
				return
			}

			for {
				select {
				case <-app.msgch:
					log.Infof("Stop thread %d for loop", i)
					app.tx.Close()
					return
				case tel := <-smsChan:
					// send for random number
					re := &results{[]result{}}
					err := app.submitSm(app.conf.App.Oaddr, tel, re)
					if err != nil {
						log.Errorf("Failed to send sm to %s, err: %v", tel, err)
					}
				}
			}
		}(i)
	}
	go generateSmsDaddr(tps)
}

func generateSmsDaddr(tps int) {
	var lr Counter
	var rndSeed int64
	var prefix string
	var adder int
	adder = 0
	prefix = conf.getDaddrPrefix()
	rndSeed = int64(conf.getRestPort())
	lr.Set(tps, time.Second)
	stopCh := b.Subscribe()
	for {
		select {
		case <-stopCh:
			log.Info("Stop sending sms loop")
			return
		default:
			if lr.Allow() {
				rand.Seed(rndSeed)
				rndSeed++
				var tel string
				switch len(prefix) {
				case 1:
					tel = fmt.Sprintf("%s%011d", prefix, rand.Intn(100000000000))
				case 2:
					tel = fmt.Sprintf("%s%010d", prefix, rand.Intn(10000000000))
				case 3:
					tel = fmt.Sprintf("%s%09d", prefix, rand.Intn(1000000000))
				case 4:
					tel = fmt.Sprintf("%s%08d", prefix, rand.Intn(100000000))
				case 5:
					tel = fmt.Sprintf("%s%07d", prefix, rand.Intn(10000000))
				case 6:
					tel = fmt.Sprintf("%s%06d", prefix, rand.Intn(1000000))
				case 7:
					tel = fmt.Sprintf("%s%05d", prefix, adder)
					adder++
					if adder >= 1000000 {
						adder = 0
					}
				default:
					tel = fmt.Sprintf("138%09d", rand.Intn(1000000000))
				}
				smsChan <- tel
			} else {
				time.Sleep(time.Millisecond)
			}
		}
	}
}

func handlerAT(p pdu.Body) {
	log.Debugf("receive AT, ID: %s, Status: %s", p.Header().ID.String(), p.Header().Status.Error())
	if p.Header().Status != 0x00000000 {
		m.AddFailure()
	}
	m.AddReceive()
}

func stopLoop(w http.ResponseWriter, r *http.Request) {
	log.Info("Stop performance testing.")
	b.Publish("stop")

	JSONResp(w, m.Result(), http.StatusOK)
}

func status(w http.ResponseWriter, r *http.Request) {
	JSONResp(w, m.Result(), http.StatusOK)
}

func resetStatus(w http.ResponseWriter, r *http.Request) {
	m.Reset()
	JSONResp(w, m.Result(), http.StatusOK)
}

func init() {
	SetupLogger()
	flag.BoolVar(&h, "h", false, "this help")
	flag.StringVar(&c, "c", "conf.yaml", "set configuration `file`")
}

func main() {
	flag.Parse()
	if h {
		flag.Usage()
		return
	}
	m = &stat{}

	smsChan = make(chan string)
	conf = &config{}
	if err := conf.init(c); err != nil {
		log.Fatalf("Failed to parse config file. %v", err)
	}
	conf.setLogLevel()
	log.Debug(conf.ToString())
	conf.startMetrics(m)
	// start broker for broadcast
	b = NewBroker()
	go b.Start()

	var app *smppApp
	if conf.isReceiver() {
		for i := 0; i < conf.getConnNum(); i++ {
			go func(i int) {
				log.Infof("Start smpp receiver %d", i)
				recv := &smppApp{conf: conf, msgch: b.Subscribe()}
				if err := recv.bindSmpp(); err != nil {
					log.Errorf("Failed to bind smpp receiver %d", i)
				} else {
					log.Infof("Success to bind smpp receiver %d", i)
				}
			}(i)
		}
	} else {
		// create basic service
		app = &smppApp{conf: conf, msgch: b.Subscribe()}

		if conf.GetAppVersion() == 2 {
			app.bindTransmiterV2()
		} else {
			if err := app.bindSmpp(); err != nil {
				log.Fatalf("Failed to bind smpp app %v", err)
			}
		}
	}

	// start connection status check in gorouting
	if !conf.isReceiver() && conf.GetAppVersion() == 1 {
		go app.checkStatus()
	}

	// example of sender handler func
	if !conf.isReceiver() && conf.GetAppVersion() == 1 {
		http.HandleFunc("/sendsms", app.handleSms)
		http.HandleFunc("/sendList", app.handleList)
		http.HandleFunc("/startLoop", startLoop)
		http.HandleFunc("/stopLoop", stopLoop)
	}

	if conf.GetAppVersion() == 2 {
		log.Debug("Start v2 app")
		http.HandleFunc("/v2/sendsms", app.handleSmsV2)
	}

	http.HandleFunc("/status", status)
	http.HandleFunc("/resetStatus", resetStatus)
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", conf.Rest.Port), nil))
}
