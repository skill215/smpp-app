package main

import (
	"crypto/hmac"
	"crypto/sha1"
	"encoding/hex"
)

var seq uint8

func genereateTid() {
	if seq == 255 {
		seq = 0
	}
	seq += 1
}

func GenerateWSPHeader(imsiBcd []byte, wbxml []byte) []byte {
	genereateTid()
	wspHeader := make([]byte, 9)
	wspHeader[0] = seq  // Transaction ID
	wspHeader[1] = 0x06 // PDU Type PDU Type (0x06 = push, WAP-230-WSP 8.2.4.1)
	wspHeader[2] = 0x2f // Content Length: 0x2f = 47 (include all content type and headers)
	wspHeader[3] = 0x1f // Length-Quote. Length > 30 and necessary to quote. (WAP-230-WSP 8.4.2.2)
	wspHeader[4] = 0x2d // Length: 0x2D = 45 (includes content type and security)
	wspHeader[5] = 0xb6 // WSP Content type (0x80 | 0x36 = B6) (application/vnd.wap.connectivity-wbxml)
	wspHeader[6] = 0x91 // SEC (0x80 | 0x11 = 0x91) (WAP-230-WSP Table 38)
	wspHeader[7] = 0x80 // NETWPIN (0x80 | 0x00)  security method
	wspHeader[8] = 0x92 // MAC (0x80 | 0x12 = 0x92) (WAP-230-WSP Table 38)

	// use imsi bcd code as secret key
	hash := hmac.New(sha1.New, imsiBcd)
	// use xbxml as data
	hash.Write(wbxml)
	mac := []byte(hex.EncodeToString(hash.Sum(nil))) // MAC value always 40 bytes
	// add end byte
	EOF := []byte{0x00}
	r := append(wspHeader, mac...)
	r = append(r, EOF...)
	return r
}
