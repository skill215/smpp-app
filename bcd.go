package main

import (
	"fmt"
	"io"
)

// BCD is the configuration for Binary-Coded Decimal encoding.
type BCD struct {
	// Map of symbols to encode and decode routines.
	// Example:
	//    key 'a' -> value 0x9
	Map map[byte]byte

	// If true nibbles (4-bit part of a byte) will
	// be swapped, meaning bits 0123 will encode
	// first digit and bits 4567 will encode the
	// second.
	SwapNibbles bool

	// Filler nibble is used if the input has odd
	// number of bytes. Then the output's final nibble
	// will contain the specified nibble.
	Filler byte
}

var (
	StandardBCD = &BCD{
		Map: map[byte]byte{
			'0': 0x0, '1': 0x1, '2': 0x2, '3': 0x3,
			'4': 0x4, '5': 0x5, '6': 0x6, '7': 0x7,
			'8': 0x8, '9': 0x9, 'f': 0xf,
		},
		SwapNibbles: false,
		Filler:      0xf}

	ErrBadInput = fmt.Errorf("non-encodable data")
	// ErrBadBCD returned if input data cannot be decoded.
	ErrBadBCD = fmt.Errorf("Bad BCD data")
)

func GenImsiBcd(imsi string) []byte {
	// if imsi has odd len, add f to the end
	input := imsi
	var headerByte byte = 0x09 // 0b1001 for odd length
	if len(imsi)%2 == 0 {
		input = imsi + "f"
		headerByte = 0x01
	}
	outLen := (len(input) + 1) / 2
	bcd := make([]byte, outLen)
	for i := 0; i < outLen; i++ {
		if i == 0 {
			bcd[0] = (StandardBCD.Map[input[0]] << 4) + headerByte&0xf
		} else {
			bcd[i] = (StandardBCD.Map[input[2*i]] << 4) + StandardBCD.Map[input[2*i-1]]&0xf
		}
	}

	return bcd
}

// Encoder is used to encode decimal string into BCD bytes.
//
// Encoder may be copied with no side effects.
type Encoder struct {
	// symbol to nibble mapping; example:
	// '*' -> 0xA
	// the value > 0xf means no mapping, i.e. invalid symbol
	hash [0x100]byte

	// nibble used to fill if the number of bytes is odd
	filler byte

	// if true the 0x45 translates to '54' and vice versa
	swap bool
}

func checkBCD(config *BCD) bool {
	nibbles := make(map[byte]bool)
	// check all nibbles
	for _, nib := range config.Map {
		if _, ok := nibbles[nib]; ok || nib > 0xf {
			// already in map or not a nibble
			return false
		}
		nibbles[nib] = true
	}
	return config.Filler <= 0xf
}

func newHashEnc(config *BCD) (res [0x100]byte) {
	for i := 0; i < 0x100; i++ {
		c, ok := config.Map[byte(i)]
		if !ok {
			// no matching symbol
			c = 0xff
		}
		res[i] = c
	}
	return
}

// NewEncoder creates new Encoder from BCD configuration.  If the
// configuration is invalid NewEncoder will panic.
func NewEncoder(config *BCD) *Encoder {
	if !checkBCD(config) {
		panic("BCD table is incorrect")
	}
	return &Encoder{
		hash:   newHashEnc(config),
		filler: config.Filler,
		swap:   config.SwapNibbles}
}

func (enc *Encoder) packNibs(nib1, nib2 byte) byte {
	if enc.swap {
		return (nib2 << 4) + nib1&0xf
	} else {
		return (nib1 << 4) + nib2&0xf
	}
}

func (enc *Encoder) pack(w []byte) (n int, b byte, err error) {
	var nib1, nib2 byte
	switch len(w) {
	case 0:
		n = 0
		return
	case 1:
		n = 1
		if nib1, nib2 = enc.hash[w[0]], enc.filler; nib1 > 0xf {
			err = ErrBadInput
		}
	default:
		n = 2
		if nib1, nib2 = enc.hash[w[0]], enc.hash[w[1]]; nib1 > 0xf || nib2 > 0xf {
			err = ErrBadInput
		}
	}
	return n, enc.packNibs(nib1, nib2), err
}

// EncodedLen returns amount of space needed to store bytes after
// encoding data of length x.
func EncodedLen(x int) int {
	return (x + 1) / 2
}

// Encode get input bytes from src and encodes them into BCD data.
// Number of encoded bytes and possible error is returned.
func (enc *Encoder) Encode(dst, src []byte) (n int, err error) {
	var b byte
	var wid int

	for n < len(dst) {
		wid, b, err = enc.pack(src)
		switch {
		case err != nil:
			return
		case wid == 0:
			return
		}
		dst[n] = b
		n++
		src = src[wid:]
	}
	return
}

// Codec encapsulates both Encoder and Decoder.
type Codec struct {
	Encoder
}

// NewCodec returns new copy of Codec. See NewEncoder and NewDecoder
// on behaviour specifics.
func NewCodec(config *BCD) *Codec {
	return &Codec{*NewEncoder(config)}
}

// Writer encodes input data and writes it to underlying io.Writer.
// Please pay attention that due to ambiguity of encoding process
// (encoded octet may indicate the end of data by using the filler
// nibble) Writer will not write odd remainder of the encoded input
// data if any until the next octet is observed.
type Writer struct {
	*Encoder
	dst  io.Writer
	err  error
	word []byte
}

// NewWriter creates new Writer with underlying io.Writer.
func (enc *Encoder) NewWriter(wr io.Writer) *Writer {
	return &Writer{enc, wr, nil, make([]byte, 0, 2)}
}

// Write implements io.Writer interface.
func (w *Writer) Write(p []byte) (n int, err error) {
	if len(p) == 0 {
		return 0, nil
	}

	// if we have remaining byte from previous run
	// join it with one of new input and encode
	if len(w.word) == 1 {
		x := append(w.word, p[0])
		_, b, err := w.pack(x)
		if err != nil {
			return 0, err
		}
		if _, err = w.dst.Write([]byte{b}); err != nil {
			return 0, err
		}
		w.word = w.word[:0]
		n += 1
	}

	// encode even number of bytes
	for len(p[n:]) >= 2 {
		_, b, err := w.pack(p[n : n+2])
		if err != nil {
			return n, err
		}
		if _, err = w.dst.Write([]byte{b}); err != nil {
			return n, err
		}
		n += 2
	}

	// save remainder
	if len(p[n:]) > 0 { // == 1
		w.word = append(w.word, p[n])
		n += 1
	}

	return
}

// Encodes all backlogged data to underlying Writer.  If number of
// bytes is odd, the padding fillers will be applied. Because of this
// the main usage of Flush is right before stopping Write()-ing data
// to properly finalize the encoding process.
func (w *Writer) Flush() error {
	if len(w.word) == 0 {
		return nil
	}
	n, b, err := w.pack(w.word)
	w.word = w.word[:0]
	if err != nil {
		// panic("hell")
		return err
	}
	if n == 0 {
		return nil
	}
	_, err = w.dst.Write([]byte{b})
	return err
}

// Buffered returns the number of bytes stored in backlog awaiting for
// its pair.
func (w *Writer) Buffered() int {
	return len(w.word)
}
