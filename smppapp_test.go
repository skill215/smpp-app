package main

import (
	"bytes"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"strconv"
	"strings"
	"testing"

	"github.com/skill215/go-smpp/smpp/pdu"
	"github.com/skill215/go-smpp/smpp/pdu/pdufield"
	"github.com/skill215/go-smpp/smpp/smpptest"
	"github.com/stretchr/testify/assert"
)

func TestSms(t *testing.T) {
	app := generateApp(t)

	req := httptest.NewRequest(http.MethodPost, "/sendsms?msisdn=123456789", nil)
	w1 := httptest.NewRecorder()
	app.handleSms(w1, req)
	res := w1.Result()
	defer res.Body.Close()
	data, err := ioutil.ReadAll(res.Body)
	assert.NoError(t, err)
	assert.Equal(t, 200, res.StatusCode)
	assert.Contains(t, string(data), "smppapp-test")

	req = httptest.NewRequest(http.MethodPost, "/sendsms?msisdn=987654321", nil)
	w2 := httptest.NewRecorder()
	app.handleSms(w2, req)
	assert.Equal(t, 400, w2.Result().StatusCode)
}

func TestSendList(t *testing.T) {
	app := generateApp(t)

	tl := telList{Inputs: []MsisdnImsi{{"123456789", "111111"}, {"123456789", "222222"}, {"987654312", "333333"}, {"111222333", "444444"}}}
	body, _ := json.Marshal(tl)
	req := httptest.NewRequest(http.MethodPost, "/sendList", bytes.NewReader(body))
	w1 := httptest.NewRecorder()
	app.handleList(w1, req)
	res := w1.Result()
	defer res.Body.Close()
	data, err := ioutil.ReadAll(res.Body)
	assert.NoError(t, err)
	result := results{}
	err = json.Unmarshal(data, &result)
	assert.NoError(t, err)
	assert.Equal(t, 200, res.StatusCode)
	assert.Equal(t, len(result.Results), 4)
	assert.Equal(t, result.Results[0].Status, "success")
	assert.Equal(t, result.Results[1].Status, "success")
	assert.Equal(t, result.Results[2].Status, "failed")
	assert.Equal(t, result.Results[3].Status, "failed")
}

func TestWithOutMsisdn(t *testing.T) {
	app := generateApp(t)
	req := httptest.NewRequest(http.MethodPost, "/sendsms", nil)
	w1 := httptest.NewRecorder()
	app.handleSms(w1, req)
	res := w1.Result()
	defer res.Body.Close()
	data, err := ioutil.ReadAll(res.Body)
	assert.NoError(t, err)
	assert.Equal(t, 400, res.StatusCode)
	assert.Contains(t, string(data), "Invalid msisdn")
}

func TestWrongJsonFormat(t *testing.T) {
	app := generateApp(t)
	req := httptest.NewRequest(http.MethodPost, "/sendList", nil)
	w1 := httptest.NewRecorder()
	app.handleList(w1, req)
	res := w1.Result()
	defer res.Body.Close()
	data, err := ioutil.ReadAll(res.Body)
	assert.NoError(t, err)
	assert.Equal(t, 400, res.StatusCode)
	assert.Contains(t, string(data), "Request format error")
}

func TestWithWrongMethod(t *testing.T) {
	app := generateApp(t)
	req := httptest.NewRequest(http.MethodGet, "/sendList", nil)
	w1 := httptest.NewRecorder()
	app.handleList(w1, req)
	res := w1.Result()
	defer res.Body.Close()
	data, err := ioutil.ReadAll(res.Body)
	assert.NoError(t, err)
	assert.Equal(t, 404, res.StatusCode)
	assert.Contains(t, string(data), "Method GET is not supported by smppapp")
}

func TestBindWithWrongCredential(t *testing.T) {
	s := getSmppServer(handleSubmitSmCorrectly)
	conf := generateWrongCredConf(s)
	app := smppApp{conf: conf}

	err := app.bindSmpp()
	assert.Error(t, err)
}

func TestBindWithTls(t *testing.T) {
	app := generateTlsApp(t)

	req := httptest.NewRequest(http.MethodPost, "/sendsms?msisdn=123456789", nil)
	w1 := httptest.NewRecorder()
	app.handleSms(w1, req)
	res := w1.Result()
	defer res.Body.Close()
	data, err := ioutil.ReadAll(res.Body)
	assert.NoError(t, err)
	assert.Equal(t, 200, res.StatusCode)
	assert.Contains(t, string(data), "smppapp-test")
}

func TestGenerateImsiBcd(t *testing.T) {
	imsi_1 := "310170212226432"
	imsi_2 := "310170212226"
	var expect_1 []byte = []byte{0x39, 0x01, 0x71, 0x20, 0x21, 0x22, 0x46, 0x23}
	var expect_2 []byte = []byte{0x31, 0x01, 0x71, 0x20, 0x21, 0x22, 0xF6}
	bcd_1 := GenImsiBcd(imsi_1)
	bcd_2 := GenImsiBcd(imsi_2)
	assert.EqualValues(t, expect_1, bcd_1)
	assert.EqualValues(t, expect_2, bcd_2)
}

func TestGenerateUDH(t *testing.T) {
	srcPort := 2948
	var h, l uint8 = uint8(srcPort >> 8), uint8(srcPort & 0xff)
	fmt.Printf("high %x, low %x", h, l)
}

func generateApp(t *testing.T) *smppApp {
	s := getSmppServer(handleSubmitSmCorrectly)
	conf := generateConf(s)

	app := smppApp{conf: conf}
	err := app.bindSmpp()
	assert.NoError(t, err)
	return &app
}

func generateTlsApp(t *testing.T) *smppApp {
	s := getSmppTlsServer(handleSubmitSmCorrectly)
	conf := generateTlsConf(s)

	app := smppApp{conf: conf}
	err := app.bindSmpp()
	assert.NoError(t, err)
	return &app
}

func getSmppServer(handler smpptest.HandlerFunc) *smpptest.Server {
	s := smpptest.NewUnstartedServer()
	s.Handler = handler
	s.Start()
	return s
}

func getSmppTlsServer(handler smpptest.HandlerFunc) *smpptest.Server {
	s := smpptest.NewUnstartedServer()
	s.TLS = &tls.Config{
		MinVersion:         tls.VersionTLS10,
		MaxVersion:         tls.VersionTLS13,
		InsecureSkipVerify: true,
	}
	s.Handler = handler
	s.Start()
	return s
}

func generateTlsConf(server *smpptest.Server) *config {
	conf := &config{}
	addr := strings.Split(server.Addr(), ":")
	conf.Smpp.Addr = addr[0]
	conf.Smpp.User = smpptest.DefaultUser
	conf.Smpp.Password = smpptest.DefaultPasswd
	port, _ := strconv.Atoi(addr[1])
	conf.Smpp.Port = port
	conf.Rest.Port = 8888
	conf.Smpp.Tls.Enable = true
	conf.Smpp.Tls.MinVersion = "1.0"
	conf.Smpp.Tls.MaxVersion = "1.3"
	return conf
}

func handleSubmitSmCorrectly(c smpptest.Conn, p pdu.Body) {
	switch p.Header().ID {
	case pdu.SubmitSMID:
		r := pdu.NewSubmitSMResp()
		r.Header().Seq = p.Header().Seq
		if val, ok := p.Fields()[pdufield.DestinationAddr]; ok && val.String() == "123456789" {
			r.Fields().Set(pdufield.MessageID, "smppapp-test") // nolint:errcheck
			r.Header().Status = 0x00000000
			c.Write(r) //nolint:errcheck
		} else {
			r.Header().Status = 0x00000008
			c.Write(r) //nolint:errcheck
		}
	default:
		smpptest.EchoHandler(c, p)
	}
}

func generateConf(server *smpptest.Server) *config {
	conf := &config{}
	addr := strings.Split(server.Addr(), ":")
	conf.Smpp.Addr = addr[0]
	conf.Smpp.User = smpptest.DefaultUser
	conf.Smpp.Password = smpptest.DefaultPasswd
	port, _ := strconv.Atoi(addr[1])
	conf.Smpp.Port = port
	conf.Rest.Port = 8888
	conf.Smpp.Tls.Enable = false
	return conf
}

func generateWrongCredConf(server *smpptest.Server) *config {
	conf := &config{}
	addr := strings.Split(server.Addr(), ":")
	conf.Smpp.Addr = addr[0]
	conf.Smpp.User = smpptest.DefaultUser
	conf.Smpp.Password = "WrongPassword"
	port, _ := strconv.Atoi(addr[1])
	conf.Smpp.Port = port
	conf.Rest.Port = 8888
	conf.Smpp.Tls.Enable = false
	return conf
}
