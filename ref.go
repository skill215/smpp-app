package main

import "sync/atomic"

var (
	seqence uint32
)

func GetSeq() uint8 {
	return uint8(atomic.AddUint32(&seqence, 1))
}
