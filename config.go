package main

import (
	"crypto/tls"
	"crypto/x509"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"log/syslog"
	"os"
	"strings"
	"time"

	"github.com/rcrowley/go-metrics"
	"github.com/rcrowley/go-metrics/stathat"
	"github.com/sirupsen/logrus"
	"gopkg.in/yaml.v2"
)

var omacp []byte

// set the loglevel for logger base on config
func (c *config) setLogLevel() {
	switch strings.ToLower(c.App.LogLevel) {
	case "info":
		logrus.SetLevel(logrus.InfoLevel)
	case "debug":
		logrus.SetLevel(logrus.DebugLevel)
	case "warn":
		logrus.SetLevel(logrus.WarnLevel)
	case "trace":
		logrus.SetLevel(logrus.TraceLevel)
	case "error":
		logrus.SetLevel(logrus.ErrorLevel)
	case "fatal":
		logrus.SetLevel(logrus.FatalLevel)
	case "panic":
		logrus.SetLevel(logrus.PanicLevel)
	default:
		logrus.SetLevel(logrus.InfoLevel)
	}
}

// read conf.yaml into config
func (c *config) init(path string) error {
	yamlFile, err := ioutil.ReadFile(path)
	if err != nil {
		logrus.Fatalf("Failed to open config file, %v", err)
		return fmt.Errorf("Failed to open config file %v ", err)
	}
	err = yaml.Unmarshal(yamlFile, &c)
	if err != nil {
		logrus.Fatalf("Failed to parse config file %v", err)
		return fmt.Errorf("Failed to parse config file %v", err)
	}
	if c.hasDataSm() {
		return c.loadOmacp()
	} else {
		return nil
	}
}

func (c *config) getMinTLSVersion() uint16 {
	return c.getTLSVersion(c.Smpp.Tls.MinVersion)
}

func (c *config) getMaxTLSVersion() uint16 {
	return c.getTLSVersion(c.Smpp.Tls.MaxVersion)
}

func (c *config) isReceiver() bool {
	return strings.EqualFold(c.Smpp.Type, "receiver")
}

func (c *config) SmppType() string {
	switch c.Smpp.Type {
	case "receiver":
		return "receiver"
	case "transceiver":
		return "transceiver"
	default:
		return "transmiter"
	}
}

func (c *config) getConnNum() int {
	if c.Smpp.ConnNum <= 0 || c.Smpp.ConnNum >= 20 {
		return 1
	} else {
		return c.Smpp.ConnNum
	}
}

func (c *config) getRestPort() int {
	return c.Rest.Port
}

func (c *config) getDaddrPrefix() string {
	if len(c.App.DaddrPrefix) > 7 {
		logrus.Infof("The DaddrPrefix is too long (%v), take only the leading 7 digits.", len(c.App.DaddrPrefix))
		return c.App.DaddrPrefix[:7]
	} else {
		logrus.Infof("The DaddrPrefix is %s.", c.App.DaddrPrefix)
		return c.App.DaddrPrefix
	}
}

func (c *config) getTLSVersion(version string) uint16 {
	switch version {
	case "1.3":
		return tls.VersionTLS13
	case "1.2":
		return tls.VersionTLS12
	case "1.1":
		return tls.VersionTLS11
	case "1.0":
		return tls.VersionTLS10
	default:
		logrus.Errorf("Unknown TLS version. Should be one of [1.0, 1.1, 1.2, 1.3]")
		logrus.Warnf("Unknown TLS version %s, set to default version 1.0", version)
		return 0
	}
}

func (c *config) isKeyCertSet() bool {
	return len(c.Smpp.Tls.Cert) > 0 && len(c.Smpp.Tls.Key) > 0
}

func (c *config) setKeyCert(tlsConf *tls.Config) {
	if c.isKeyCertSet() {
		logrus.Infof("Set TLS certificate %s and Key %s", c.Smpp.Tls.Cert, c.Smpp.Tls.Key)
		cert, err := tls.LoadX509KeyPair(c.Smpp.Tls.Cert, c.Smpp.Tls.Key)
		if err != nil {
			logrus.Errorf("Failed to load key cert pair for TLS %v", err)
			panic("Cannot load key and cert for TLS")
		}
		tlsConf.Certificates = []tls.Certificate{cert}
	}
}

func (c *config) isCipherSet() bool {
	return len(c.Smpp.Tls.Ciphers) > 0
}

func (c *config) setCiphers(tlsConf *tls.Config) {
	if c.isCipherSet() {
		logrus.Infof("Set TLS cipher suites %s", c.Smpp.Tls.Ciphers)
		cipherMap := make(map[string]uint16)
		cipherList := []uint16{}
		for _, cipher := range tls.CipherSuites() {
			cipherMap[cipher.Name] = cipher.ID
		}
		ciphers := strings.Split(c.Smpp.Tls.Ciphers, ",")
		for _, c := range ciphers {
			cipher := strings.TrimSpace(c)
			if val, ok := cipherMap[cipher]; ok {
				cipherList = append(cipherList, val)
			} else {
				logrus.Errorf("cipher %s not supported", c)
			}
		}
		if len(cipherList) > 0 {
			tlsConf.CipherSuites = cipherList
		}
	}
}

func (c *config) isCaCertSet() bool {
	return len(c.Smpp.Tls.CaCert) > 0
}

func (c *config) setCaCert(tlsConf *tls.Config) {
	if c.isCaCertSet() && c.isKeyCertSet() {
		logrus.Infof("Load TLS CA cert %s", c.Smpp.Tls.CaCert)
		caCert, err := ioutil.ReadFile(c.Smpp.Tls.CaCert)
		if err != nil {
			logrus.Errorf("Failed to read ca certificate. err: %v", err)
			panic("Failed to read ca certificate")
		}
		caCertPool := x509.NewCertPool()
		caCertPool.AppendCertsFromPEM(caCert)
		tlsConf.RootCAs = caCertPool
	}
}

func (c *config) setTLSServerName(tlsConf *tls.Config) {
	if len(c.Smpp.Tls.ServerName) > 0 {
		tlsConf.ServerName = c.Smpp.Tls.ServerName
	}
}

func (c *config) isTlsEnabled() bool {
	return c.Smpp.Tls.Enable
}

func (c *config) isServerCertVerified() bool {
	return c.Smpp.Tls.VerifyServer
}

func (c *config) generateTlsConf() *tls.Config {
	tlsConf := &tls.Config{
		InsecureSkipVerify: !c.Smpp.Tls.VerifyServer,
		MinVersion:         c.getMinTLSVersion(),
		MaxVersion:         c.getMaxTLSVersion(),
		// KeyLogWriter:       os.Stdout,
	}
	if c.isServerCertVerified() {
		c.setCaCert(tlsConf)
		c.setTLSServerName(tlsConf)
	}
	c.setKeyCert(tlsConf)
	c.setCiphers(tlsConf)
	return tlsConf
}

func (c *config) loadOmacp() error {
	content, err := ioutil.ReadFile(c.Content.OmacpPath)
	if err != nil {
		// fmt.Printf("Can't open omacp file. Please check it. %v", err)
		return err
	}
	omacp = content
	return nil
}

func (c *config) GetOmacp() []byte {
	return omacp
}

func (c *config) GetAppVersion() int {
	switch c.App.Version {
	case 2:
		return 2
	default:
		return 1
	}
}

func (c *config) startMetrics(m *stat) {
	if c.SmppType() != "transmiter" {
		m.startReceiverMetrics()
	} else {
		m.startSenderMetrics()
	}
	switch c.Metrics.Output {
	case "log":
		f, _ := os.OpenFile("stat.log", os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0644)
		go metrics.Log(metrics.DefaultRegistry, time.Duration(c.Metrics.Interval)*time.Second, log.New(f, "", log.Lmicroseconds))
	case "stathat":
		go stathat.Stathat(metrics.DefaultRegistry, time.Duration(c.Metrics.Interval)*time.Second, c.Metrics.User)
	case "syslog":
		w, _ := syslog.Dial("unixgram", "/dev/log", syslog.LOG_INFO, "metrics")
		go metrics.Syslog(metrics.DefaultRegistry, time.Duration(c.Metrics.Interval)*time.Second, w)
	case "stderr":
		go metrics.Log(metrics.DefaultRegistry, time.Duration(c.Metrics.Interval)*time.Second, logrus.New())
	default:
		return
	}
}

func (c *config) ToString() string {
	return fmt.Sprintf("%+v", *c)
}

func (c *config) hasDataSm() bool {
	_, err := os.Stat(c.Content.OmacpPath)
	return !errors.Is(err, os.ErrNotExist)
}

func (c *config) hasMessage() bool {
	return len(c.Content.Message) == 0
}

func (c *config) authenticate(user, password string) bool {
	if c.Rest.User == "" {
		return true
	} else {
		return user == c.Rest.User && password == c.Rest.Password
	}
}

func (c *config) getSrcPort() uint16 {
	if c.Content.WapPush.SourcePort == 0 {
		return 2948
	}
	return c.Content.WapPush.SourcePort
}

func (c *config) getDestPort() uint16 {
	if c.Content.WapPush.DestPort == 0 {
		return 2948
	}
	return c.Content.WapPush.DestPort
}
